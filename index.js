const express = require('express')
const bodyParser = require('body-parser')
const shortener = require('./src/shortener')
const expressSanitizer = require('express-sanitizer')
const enforce = require('express-sslify')

const app = express()
const port = 3000

app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
// disabled locally
// app.use(enforce.HTTPS())

app.use(expressSanitizer())

app.get('/:alias', (req, res) => {
  shortener
    .resolve(req.params.alias)
    .then(originalUrl => res.redirect(originalUrl))
    .catch(err => {
      res.status(404).send({ message: err.message })
    })
})

app.post('/short', (req, res) => {
  if (req.body) {
    shortener
      .shorten(req.body.url, req.body.alias)
      .then(responsePayload => {
        res.status(201).send(responsePayload)
      })
      .catch(err => {
        res.status(500).send({ message: err.message })
      })
  } else res.status(400).send({ message: 'Missing body parameter' })
})

app.listen(port, () => console.log(`Shortner listening on port ${port}!`))

module.exports = { app: app }
