// this is ideally a process.env variable pointing at the right domain, like https://shortner.com
// the HTTPS doesn't run locally unless you create a self signe OpenSSL certificate.
// Everything is SSL ready, the express-sslify module forces SSL in case you run on a SSL enabled server
const baseUrl = 'http://localhost:3000/' 

// any other library creating a unique id works. Many solutions available like cryptojs or mini-id, but I didn't want to include dependencies for the PoC
const randomAlias = () => {
  return Math.random()
    .toString(36)
    .substr(2, 6)
}

// Ideally this is a MongoDB or any other desired DB to persist the urls and their aliases
// I would add on top of it a shared Redis to let all the services running on top of this able to resolve the shortened url into the original one
// I'll use a simple Map to avoid duplicates and access quickly to the values (it should be average case O(1) but it's not guaranteed. Worst case O(N).)

const urlMap = new Map()

module.exports = {
  shorten: (url, alias) => {
    if (!url) return Promise.reject(new Error('Missing URL parameter'))

    let payload = { originalUrl: url }
    if (alias === '' || (alias != null && alias.trim() === '') || alias == null) {
      payload.alias = randomAlias()
    } else if (urlMap.has(alias)) {
      payload.alias = randomAlias()
      payload.message =
        'Sorry, we had to give you a random alias because yours was not available.'
    } else {
      payload.alias = alias
    }

    urlMap.set(payload.alias, url)
    payload.shortUrl = baseUrl + payload.alias
    console.log(`${url} shortened as ${payload.shortUrl}`)
    return Promise.resolve(payload)
  },
  resolve: alias => {
    if (urlMap.has(alias)) {
      return Promise.resolve(urlMap.get(alias))
    }
    return Promise.reject(new Error('URL not found'))
  }
}
