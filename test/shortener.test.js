const chai = require('chai')
const expect = chai.expect
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised).should()

const shortener = require('../src/shortener')

describe('Shortener test', () => {
  it('should return Promise.reject if body.url is missing', function () {
    expect(shortener.shorten()).to.eventually.be.rejected
  })

  it('should return Promise.reject if body.url is missing with specific message "Missing URL parameters', function () {
    return shortener.shorten().catch(err => {
      expect(err.message).to.equal('Missing URL parameter')
    })
  })

  it('should return a payload with originalUrl and an alias', function () {
    return shortener.shorten('http://test.com').then(payload => {
      expect(payload.originalUrl).to.equal('http://test.com')
      expect(payload.alias).to.be.not.empty
    })
  })

  it('should return a payload with originalUrl and the specified alias', function () {
    return shortener.shorten('http://test.com', 'myAlias').then(payload => {
      expect(payload.originalUrl).to.equal('http://test.com')
      expect(payload.alias).to.equal('myAlias')
    })
  })

  it('should return a payload with originalUrl, an alias and a message saying the alias is taken', function () {
    return shortener.shorten('http://test.com', 'myAlias').then(payload => {
      expect(payload.originalUrl).to.equal('http://test.com')
      expect(payload.alias).to.not.be.equal('myAlias')
      expect(payload.message).to.equal(
        'Sorry, we had to give you a random alias because yours was not available.'
      )
    })
  })
})
