const chai = require('chai')
const expect = chai.expect
const chaiAsPromised = require('chai-as-promised')
const request = require('supertest')
const app = require('../index').app
chai.use(chaiAsPromised).should()

describe('Shortener test', () => {
  it('should return 404 if the alias parameter is empty', function (done) {
    request(app)
      .get('/')
      .expect(404)
      .end(done)
  })

  it('should return 500 for missing body parameter', function (done) {
    request(app)
      .post('/short')
      .expect(500)
      .end((err, res) => {
        expect(JSON.parse(res.error.text).message).to.equal(
          'Missing URL parameter'
        )
        done()
      })
  })

  it('should return a shortened url', function (done) {
    request(app)
      .post('/short')
      .send({ url: 'https://appsynth.net/', alias: 'test1' })
      .expect(201)
      .end((err, res) => {
        expect(res.body.originalUrl).to.equal('https://appsynth.net/')
        expect(res.body.alias).to.be.equal('test1')
        expect(res.body.shortUrl).to.be.not.empty
        done()
      })
  })

  it('should redirect to the original url', function (done) {
    request(app)
      .get('/test1')
      .expect(302) // redirection code
      .end((err, res) => {
        expect(res.header.location).to.equal('https://appsynth.net/')
        done()
      })
  })
})
